Summary: Tracks and displays system calls associated with a running process
Name: strace
Version: 6.6
Release: 3
# The test suite is GPLv2+, all the rest is LGPLv2.1+.
License: LGPL-2.1+ and GPL-2.0+
# Some distros require Group tag to be present,
# some require Group tag to be absent,
# some do not care about Group tag at all,
# and we have to cater for all of them.
URL: https://strace.io
Source: https://strace.io/files/%{version}/strace-%{version}.tar.xz
BuildRequires: xz
BuildRequires: gcc gzip make

# Install Bluetooth headers for AF_BLUETOOTH sockets decoding.
BuildRequires: pkgconfig(bluez)

# Install elfutils-devel or libdw-devel to enable strace -k option.
# Install binutils-devel to enable symbol demangling.
BuildRequires: elfutils-devel binutils-devel

Patch0001: backport-0001-Fix-the-type-of-tcbtab-iterators.patch
Patch0002: backport-0002-Move-print_debug_info-definition-before-cleanup.patch
Patch0003: backport-0003-Factor-out-droptcb_verbose-from-detach.patch
Patch0004: backport-0004-Factor-out-interrupt_or_stop-from-detach.patch
Patch0005: backport-0005-Factor-out-detach_interrupted_or_stopped-from-detach.patch
Patch0006: backport-0006-strace-fix-potential-deadlock-during-cleanup.patch

# OBS compatibility
%{?!buildroot:BuildRoot: %_tmppath/buildroot-%name-%version-%release}
%define maybe_use_defattr %{?suse_version:%%defattr(-,root,root)}

# Fallback definitions for make_build/make_install macros
%{?!__make:       %global __make %_bindir/make}
%{?!__install:    %global __install %_bindir/install}
%{?!make_build:   %global make_build %__make %{?_smp_mflags}}
%{?!make_install: %global make_install %__make install DESTDIR="%{?buildroot}"}

%description
The strace program intercepts and records the system calls called and
received by a running process.  Strace can print a record of each
system call, its arguments and its return value.  Strace is useful for
diagnosing problems and debugging, as well as for instructional
purposes.

Install strace if you need a tool to track the system calls made and
received by a process.

%prep
%autosetup -p1 -n %{name}-%{version}
echo -n %version-%release > .tarball-version
echo -n 2022 > .year
echo -n 2022-10-16 > doc/.strace.1.in.date
echo -n 2022-01-01 > doc/.strace-log-merge.1.in.date

%build
echo 'BEGIN OF BUILD ENVIRONMENT INFORMATION'
uname -a |head -1
libc="$(ldd /bin/sh |sed -n 's|^[^/]*\(/[^ ]*/libc\.so[^ ]*\).*|\1|p' |head -1)"
$libc |head -1
file -L /bin/sh
gcc --version |head -1
ld --version |head -1
kver="$(printf '%%s\n%%s\n' '#include <linux/version.h>' 'LINUX_VERSION_CODE' | gcc -E -P -)"
printf 'kernel-headers %%s.%%s.%%s\n' $(($kver/65536)) $(($kver/256%%256)) $(($kver%%256))
echo 'END OF BUILD ENVIRONMENT INFORMATION'

export CC_FOR_BUILD="$CC";
CFLAGS_FOR_BUILD="$RPM_OPT_FLAGS"; export CFLAGS_FOR_BUILD
%configure --enable-mpers=check
%make_build

%install
%make_install

# some say uncompressed changelog files are too big
for f in ChangeLog ChangeLog-CVS; do
	gzip -9n < "$f" > "$f".gz &
done
wait

%check
#make check

#width=$(echo __LONG_WIDTH__ |%__cc -E -P -)
#skip_32bit=0
#%if 0%{?fedora} >= 35 || 0%{?rhel} > 9
#skip_32bit=1
#%endif

#if [ "${width}" != 32 ] || [ "${skip_32bit}" != 1 ]; then
#	%{buildroot}%{_bindir}/strace -V
#	%make_build -k check VERBOSE=1
#	echo 'BEGIN OF TEST SUITE INFORMATION'
#	tail -n 99999 -- tests*/test-suite.log tests*/ksysent.gen.log
#	find tests* -type f -name '*.log' -print0 |
#		xargs -r0 grep -H '^KERNEL BUG:' -- ||:
#	echo 'END OF TEST SUITE INFORMATION'
#fi

%files
%maybe_use_defattr
%doc CREDITS ChangeLog.gz ChangeLog-CVS.gz COPYING NEWS README
%{_bindir}/strace
%{_bindir}/strace-log-merge
%{_mandir}/man1/*

%changelog
* Fri Dec 27 2024 wangxiao <wangxiao184@h-partners.com> - 6.6-3
- strace: fix potential deadlock during cleanup

* Wed Feb 28 2024 liuchao <liuchao173@huawei.com> - 6.6-2
- remove redundant judgments

* Fri Jan 19 2024 liuchao <liuchao173@huawei.com> - 6.6-1
- update to 6.6 to fix compile error with kernel 6.6

* Tue Aug 8 2023 suweifeng <suweifeng1@huawei.com> - 6.1-4
- Fix compile error with kernel 6.4

* Mon Jul 10 2023 zhujin <zhujin18@huawei.com> - 6.1-3
- Fixed obs build error

* Sat May 13 2023 chenzanyu <chenzanyu@huawei.com> - 6.1-2
- Fixed A compilation error under LLVM because no compiler was specified in file strace.spec

* Fri Feb 3 2023 zhujin <zhujin18@huawei.com> - 6.1-1
- update to 6.1

* Mon Nov 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 5.14-1
- update to 5.14

* Mon Feb 1 2021 xinghe <xinghe1@huawei.com> - 5.10-1
- update to 5.10

* Tue Nov 10 2020 xinghe <xinghe1@huawei.com> - 5.6-2
- fix master build error

* Sat Jul 25 2020 liuchao<liuchao173@huawei.com> - 5.6-1
- Upgrade strace version to 5.6

* Wed Jan  8 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0-2
- Upgrade strace version to 5.0

* Tue Mar 19 2019 strace-devel@lists.strace.io - 5.0-1
- strace 5.0 snapshot.
